Código de conducta
==================

Código de conducta que aplicará inicialmente para el grupo de `Telegram <https://t.me/PythonEsp>`_  y para los proyectos dentro de este grupo de repositorios, se espera poder lograr con el tiempo que este sea un código de conducta general en español para cualquier grupo de trabajo/colaboración donde sea aplicable.

`Leer el código de conducta aqúi <codigo-de-conducta.rst>`_
